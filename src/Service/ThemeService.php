<?php


namespace App\Service;

/**
 * Service pour la gestion des thèmes
 * @package App\Service
 */

class ThemeService
{
    // Liste des thèmes
    private $themes = [
        'Cerulean',
        'Cosmo',
        'Cyborg',
        'Darkly',
        'Journal',
        'Flatly',
        'Litera',
        'Lumen',
        'Lux',
        'Sketchy',
        'Slate',
        'Solar',
        'Superhero',
        'Yeti'
    ];

    public function getThemes()
    {
        return $this->themes;
    }

    public function themRandomize(array $themes)
    {
        $themes        = $this->getThemes();
        $randomIndex   = rand(0, count($themes) -1);
        $theme         = $themes[$randomIndex];

        return $theme;
    }

}