<?php

namespace App\Controller;

use App\Entity\Genre;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Livre;

class BookController extends AbstractController
{
    /**
     * @Route("/book", name="book")
     */
    /*public function index()
    {
        return $this->render('book/index.html.twig', [
            'controller_name' => 'BookController',
        ]);
    }*/

    public function bookCatalog(EntityManagerInterface $entityManager, $codeGenre)
    {
        // récupérer l'EntityManager (injectez le manager dans la méthode)
        // et c'est tout

        // récuperer le dépôt (repository) des livres
        $rep = $entityManager->getRepository(Livre::class);

        // extraire des données de dépôt
        // select * from livres;
        //$livres = $rep->findAll();

        // SELECT * from livres WHERE id = '$refLivre
        // find() fait une recherche sur la clef primaire de la table
        // ne renvoie qu'un seul objet ou null si la clef primaire n'a pas été trouvée
        //$livre = $rep->find($refLivre);

        // SELECT * FROM livres WHERE code_genre = '$codeGenre'
        if ($codeGenre == 'TOUS')
        {
            $livres = $rep->findAll();
        } else {
            $livres = $rep->findByCodeGenre($codeGenre);
        }

        //$nouveautes = $rep->findByNouveaute();

        //extraire les genres
        $genres = $entityManager->getRepository(Genre::class)
            ->findAll();

        return $this->render(
            'books/book-list.html.twig',
            ['livres'=>$livres, 'genres' => $genres]
        );
    }

    public function ficheLivre($id, EntityManagerInterface $entityManager)
    {
        $rep = $entityManager->getRepository(Livre::class);

        $livre = $rep->find($id);

        return $this->render('books/book-detail.html.twig',
            ['livre'=>$livre]);
    }

    /*public function filtre($code, EntityManagerInterface $entityManager)
    {
        $rep = $entityManager->getRepository(Livre::class);

        $livre = $rep->find($code);

        return $this->render('books/book-list.html.twig',
            ['livre'=>$livre]);
    }*/


    public function bookCatalog2(EntityManagerInterface $entityManager)
        {

        $books = $entityManager
            ->getRepository(Livre::class)
            ->findAll();

        return $this->render(
            'books/book-list.html.twig',
            ['books'=>$books]
        );
    }

    public function bookCatalog3()
    {
        // récuperer l'EntityManager (version Symfony classique)
        $em = $this->getDoctrine()
            ->getManager();

        return $this->render('books/book-list.html.twig');
    }
}
