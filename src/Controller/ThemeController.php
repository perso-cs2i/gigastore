<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class ThemeController extends AbstractController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    // Liste des thèmes
    private $themes = [
        'Cerulean',
        'Cosmo',
        'Cyborg',
        'Darkly',
        'Journal',
        'Flatly',
        'Litera',
        'Lumen',
        'Lux',
        'Materia',
        'Minty',
        'Pulse',
        'Sandstone',
        'Simplex',
        'Sketchy',
        'Slate',
        'Solar',
        'Spacelab',
        'Superhero',
        'United',
        'Yeti'
    ];

    /**
     * Liste des thèmes
     * @return array
     */
    public function getThemes()
    {
        return $this->themes;
    }

    /**
     * Renvoyer un nom de thèmes au hasard
     * @return string  Nom de thème
     */
    public function themeRandomize()
    {
        $themes         = $this->getThemes();
        $randomIndex    = rand(0, count($themes) - 1);
        $theme          = $themes[$randomIndex];

        return $theme;
    }

    public function selectTheme(Request $request, $id)
    {
        //créer le cookie
        $cookie = new Cookie('themeCookie', $id);


        //rediriger vers l'url où on se trouvait
        $url = $request->headers->get('referer');
        $response = $this->redirect($url);
        $response->headers->setCookie($cookie);

        //renvoyer la réponse au visiteur (donc déposer le cookie)
        return $response;

    }

    public function getCurrentTheme()
    {
        return $this->requestStack
            ->getCurrentRequest()
            ->cookies
            ->get('themeCookie', 'cyborg');
    }
}
