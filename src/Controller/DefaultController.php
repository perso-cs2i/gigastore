<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }


    public function livres(EntityManagerInterface $entityManager) {

        $books = $entityManager
            ->getRepository(Livre::class)
            ->findAll();

        return $this->render('books/book-catalog.html.twig',
            [ 'books' => $books]);
    }


    public function construction()
    {
        return $this->render('default/construction.html.twig');
    }

    public function nous()
    {
        return $this->render('default/nous.html.twig');
    }

    public function contact()
    {
        return $this->render('default/contact.html.twig');
    }

    public function mentions()
    {
        return $this->render('default/mentions.html.twig');
    }

    public function cgv()
    {
        return $this->render('default/cgv.html.twig');
    }
}
