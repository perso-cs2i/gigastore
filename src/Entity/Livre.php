<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gigastore.livres
 * schema="gigastore",
 *
 * @ORM\Table(name="livres", indexes={@ORM\Index(name="ix_livres_genre", columns={"code_genre"})})
 * @ORM\Entity
 */
class Livre
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=10, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=100, nullable=false)
     */
    private $titre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="auteur", type="string", length=100, nullable=true, options={"default"="NULL"})
     */
    private $auteur = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="editeur", type="string", length=100, nullable=true, options={"default"="NULL"})
     */
    private $editeur = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="collection", type="string", length=100, nullable=true, options={"default"="NULL"})
     */
    private $collection = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="annee", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $annee = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="support", type="string", length=8, nullable=true, options={"default"="NULL"})
     */
    private $support = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="format", type="string", length=15, nullable=true, options={"default"="NULL"})
     */
    private $format = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true, options={"default"="NULL"})
     */
    private $description = 'NULL';

    /**
     * @var int
     *
     * @ORM\Column(name="pages", type="integer", nullable=false)
     */
    private $pages = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="poids", type="decimal", precision=5, scale=3, nullable=false, options={"default"="0.000"})
     */
    private $poids = '0.000';

    /**
     * @var string
     *
     * @ORM\Column(name="prix", type="decimal", precision=5, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $prix = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="port", type="decimal", precision=5, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $port = '0.00';

    /**
     * @var int|null
     *
     * @ORM\Column(name="delai_de_livraison", type="integer", nullable=true)
     */
    private $delaiDeLivraison = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="photo", type="string", length=50, nullable=true, options={"default"="NULL"})
     */
    private $photo = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="nouveaute", type="integer", nullable=true)
     */
    private $nouveaute = '0';

    /**
     * @var \Genre
     *
     * @ORM\ManyToOne(targetEntity="Genre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="code_genre", referencedColumnName="code")
     * })
     */
    private $codeGenre;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Livre
     */
    public function setId(string $id): Livre
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     * @return Livre
     */
    public function setTitre(string $titre): Livre
    {
        $this->titre = $titre;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    /**
     * @param string|null $auteur
     * @return Livre
     */
    public function setAuteur(?string $auteur): Livre
    {
        $this->auteur = $auteur;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEditeur(): ?string
    {
        return $this->editeur;
    }

    /**
     * @param string|null $editeur
     * @return Livre
     */
    public function setEditeur(?string $editeur): Livre
    {
        $this->editeur = $editeur;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCollection(): ?string
    {
        return $this->collection;
    }

    /**
     * @param string|null $collection
     * @return Livre
     */
    public function setCollection(?string $collection): Livre
    {
        $this->collection = $collection;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    /**
     * @param int|null $annee
     * @return Livre
     */
    public function setAnnee(?int $annee): Livre
    {
        $this->annee = $annee;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSupport(): ?string
    {
        return $this->support;
    }

    /**
     * @param string|null $support
     * @return Livre
     */
    public function setSupport(?string $support): Livre
    {
        $this->support = $support;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     * @return Livre
     */
    public function setFormat(?string $format): Livre
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Livre
     */
    public function setDescription(?string $description): Livre
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }

    /**
     * @param int $pages
     * @return Livre
     */
    public function setPages(int $pages): Livre
    {
        $this->pages = $pages;
        return $this;
    }

    /**
     * @return string
     */
    public function getPoids(): string
    {
        return $this->poids;
    }

    /**
     * @param string $poids
     * @return Livre
     */
    public function setPoids(string $poids): Livre
    {
        $this->poids = $poids;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrix(): string
    {
        return $this->prix;
    }

    /**
     * @param string $prix
     * @return Livre
     */
    public function setPrix(string $prix): Livre
    {
        $this->prix = $prix;
        return $this;
    }

    /**
     * @return string
     */
    public function getPort(): string
    {
        return $this->port;
    }

    /**
     * @param string $port
     * @return Livre
     */
    public function setPort(string $port): Livre
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDelaiDeLivraison(): ?int
    {
        return $this->delaiDeLivraison;
    }

    /**
     * @param int|null $delaiDeLivraison
     * @return Livre
     */
    public function setDelaiDeLivraison(?int $delaiDeLivraison): Livre
    {
        $this->delaiDeLivraison = $delaiDeLivraison;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    /**
     * @param string|null $photo
     * @return Livre
     */
    public function setPhoto(?string $photo): Livre
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getNouveaute(): ?int
    {
        return $this->nouveaute;
    }

    /**
     * @param int|null $nouveaute
     * @return Livre
     */
    public function setNouveaute(?int $nouveaute): Livre
    {
        $this->nouveaute = $nouveaute;
        return $this;
    }

    /**
     * @return Genre
     */
    public function getCodeGenre(): Genre
    {
        return $this->codeGenre;
    }

    /**
     * @param Genre $codeGenre
     * @return Livre
     */
    public function setCodeGenre(Genre $codeGenre): Livre
    {
        $this->codeGenre = $codeGenre;
        return $this;
    }



}
