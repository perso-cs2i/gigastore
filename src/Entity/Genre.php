<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gigastore.genres
 * schema="gigastore",
 *
 * @ORM\Table(name="genres")
 * @ORM\Entity
 */
class Genre
{
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=30, nullable=false)
     */
    private $intitule;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Genre
     */
    public function setCode(string $code): Genre
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getIntitule(): string
    {
        return $this->intitule;
    }

    /**
     * @param string $intitule
     * @return Genre
     */
    public function setIntitule(string $intitule): Genre
    {
        $this->intitule = $intitule;
        return $this;
    }




}
